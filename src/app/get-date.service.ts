import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetDateService {

  public currentDate: string;

  constructor() { }

  getDate() {
    const d = new Date();
    const getMonth = d.getMonth() + 1;
    const objDate = {
      year: d.getFullYear(),
      month: (getMonth < 10 ? '0' : '') + getMonth,
      day: (d.getDate() < 10 ? '0' : '') + d.getDate(),
      hour: (d.getHours() < 10 ? '0' : '') + d.getHours(),
      minute: (d.getMinutes() < 10 ? '0' : '') + d.getMinutes(),
    };
    const inputDate = objDate.year + '.' + objDate.month + '.' + objDate.day + ' | ' + objDate.hour + ':' + objDate.minute;
    return this.currentDate = inputDate;
  }

}
