import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable, observable} from 'rxjs';
import {TaskFilterService} from '../task-filter.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
})
export class TaskListComponent implements OnInit {

  @Input() taskValue;

  @Input() tasksArray;

  @Input() currentDate;

  @Input() taskListHidden;

  tasksArrayFilter: EventEmitter<any> = new EventEmitter<any>();
  arrayForSearch;
  changeFieldValue = '';
  changerWindow = false;
  delAllWindow = false;
  delWindow = false;
  taskIndex = 0;
  done = false;

  ngOnInit() {
    this.tasksArrayFilter
      .subscribe(ok => {
        this.tasksArray = this.filtered(this.arrayForSearch, 'value', ok);
        });
  }

  filtered(array: any[], property: string, search: string) {
    if (search) {
      return array.filter(obj => obj[property].toString().toLowerCase()
        .indexOf(search.toString().toLowerCase(), 0) !== -1);
    } else {
      return array;
    }
  }

  filterTasksArray(search) {
    this.arrayForSearch = JSON.parse(localStorage.getItem('taskArray'));;
    this.tasksArrayFilter.emit(search);
  }

  showDelAllWindow() {
    this.delAllWindow = true;
    console.log(this.taskIndex);
  }

  delAllTasks() {
    localStorage.setItem('taskArray', JSON.stringify([]));

    // this.tasksArray.splice(0, this.tasksArray.length);
    this.tasksArray.length = 0;
    this.taskListHidden = true;
    console.log(this.tasksArray);
    console.log(this.taskListHidden);
  }

  doneTask(i) {
    this.tasksArray[i].done = !this.tasksArray[i].done;
    localStorage.setItem('taskArray', JSON.stringify(this.tasksArray));
  }


  showDelWindow(i) {
    this.delWindow = true;
    this.taskIndex = i;
    console.log(this.taskIndex);
  }

  showChangeWindow(i) {
    this.changerWindow = true;
    this.changeFieldValue = this.tasksArray[i].value;
    this.taskIndex = i;
  }

  hideWwindow() {
    this.delWindow = false;
    this.changerWindow = false;
    this.delAllWindow = false;
  }

  removeTask(i) {
    i = this.taskIndex;
    console.log(this.tasksArray);
    this.tasksArray.splice(i, 1);
    console.log(this.tasksArray);
    localStorage.setItem('taskArray', JSON.stringify(this.tasksArray));
    this.delWindow = false;
  }

  changeTask(i) {
    i = this.taskIndex;
    console.log(i);
    if (this.tasksArray[i].value !== this.changeFieldValue) {
      this.tasksArray[i].value = this.changeFieldValue;
      localStorage.setItem('taskArray', JSON.stringify(this.tasksArray));
      this.changerWindow = false;
    } else {
      alert('Измените значение');
    }
  }

  changePriorityUp(i) {
    if (this.tasksArray[i].priority < 9) {
      ++this.tasksArray[i].priority;
      localStorage.setItem('taskArray', JSON.stringify(this.tasksArray));
    }
  }

  changePriorityDown(i) {
    if (this.tasksArray[i].priority > 1) {
      --this.tasksArray[i].priority;
      localStorage.setItem('taskArray', JSON.stringify(this.tasksArray));
    }
  }

  prioritySortingUp() {
    this.tasksArray.sort((objA, objB) => objA.priority - objB.priority);
  }

  prioritySortingDown() {
    this.tasksArray.sort((objA, objB) => objB.priority - objA.priority);
  }

  search(searchValue) {

  }
}
