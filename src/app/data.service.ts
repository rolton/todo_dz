import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {

  constructor() {
  }
  createDb() {
    let taskList = [
        { date: '21.11.2018',
          id: 0,
          value: 'Test task',
          priority: 1,
        }
    ];
    return {taskList};
  }
}
