import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GetDateService} from './get-date.service';
import {TaskFilterService} from './task-filter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements OnInit {

  public taskValue: string;
  public tasksArray: any = [];
  public taskListHidden = true;
  protected newTask = {
    id: 0,
    date: '',
    value: '',
    priority: 0,
    status: false,
  };

  constructor(private http: HttpClient,
              private getDate: GetDateService,
              private taskFilterService: TaskFilterService, ) {}

  ngOnInit() {
    this.getDataFromLS();
  }

  // work model with request to InMemoryWebApi

  /*addTask() {
    this.newTask.date = this.getDate.getDate();
    this.newTask.id = this.tasksArray.length + 1;
    this.newTask.value = this.taskValue;
    this.newTask.priority = 1;

    this.setDataToWebApi();

    console.log(this.tasksArray);
  }

  setDataToWebApi() {
    this.http.post('app/taskList', this.newTask).subscribe(
      data => {
        this.getDataFromWebApi();
      }
    );
  }

  getDataFromWebApi() {
    this.http.get('app/taskList').subscribe(
      data => {
        this.tasksArray = data;
        console.log(this.tasksArray);
      }
    );
  }*/


  addTask() {
    this.newTask.date = this.getDate.getDate();
    this.newTask.id = this.tasksArray.length + 1;
    this.newTask.value = this.taskValue;
    this.newTask.priority = 1;

    this.tasksArray.push(this.newTask);
    console.log(this.tasksArray);

    localStorage.setItem('taskArray', JSON.stringify(this.tasksArray));
    this.getDataFromLS();
    this.taskValue = '';
  }
  getDataFromLS() {
    if (localStorage.getItem('taskArray') ) {
      this.tasksArray = JSON.parse(localStorage.getItem('taskArray'));
      if (this.tasksArray.length > 0) {
      this.taskListHidden = false;
      }
      console.log(this.tasksArray);
    }
  }

}


