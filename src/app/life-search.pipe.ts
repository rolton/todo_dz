import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lifeSearch'
})
export class LifeSearchPipe implements PipeTransform {

  transform(tasksArray: any, term: any): any {
    if (term === undefined) {
      return tasksArray;
    }
    return tasksArray.filter((task) => {
      return task.value.toLowerCase().includes(term.toLowerCase());
    });
  }
}
